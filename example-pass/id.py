def id():
    id = "12345678"           # Student ID
    name = "Benjamin Millar"  # Student name
    cohort = "Electrical"     # Cohort – must be Electrical or Communications
    return (id, name, cohort)
