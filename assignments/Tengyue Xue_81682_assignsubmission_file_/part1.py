import sys
if len(sys.argv)==1:                                                                    # No argument run it.
    print("Hello world!")
elif len(sys.argv)==2:                                                                 # Only one argument can run it.
    print("Hello %s!"%(sys.argv[1]))                                           
elif len(sys.argv)==3:                                                                 # Two arguments can run it.                                                
      i=1                                                             
      while i<3:                                                                             # Using loop to cycle output  "Hello <name>" in the code.
         names=[sys.argv[i]]
         for x in names:       
             print("Hello %s !"%(x))
         i+=1
else:                                                                                           # Three arguments can run it.
      i=1
      while i<4:                                                                            #  Using loop to cycle "Hello <name>" in the code.
         names=[sys.argv[i]]
         for x in names:       
             print("Hello %s !"%(x))
         i+=1
