#!/usr/bin/env python3
#To create a file to show the amount of the word "monkey" which is found in the file.txt
f = open('file.txt',"r")  # Open the file "file.txt"
str1 = f.read() #Save all the sentences (words) in string into str1
str2 = str1.count("monkey") #To count how many "monkey" in  "file.txt"
f2 = open('monkeys.txt','w') # Create a file called "monkey.txt" to save the amount of monkey in "writing" mode
f2.write(str(str2)) #Write the amount into the file 
f2.write(" monkeys found")#Write the last thing left
