#!/usr/bin/env python3
# if no arguments,this program will print"Hello world!"
# if one argument<name> is given,it will print"Hello<name>!"
# if multiple arguements are given,it will print"Hello<name>!"for each arguement given,with one line for each name.
import sys
i=1
if len(sys.argv)==1:
    print("Hello world!")
else:
    while i<len(sys.argv):
      print("Hello %s!"%(sys.argv[i]))
      i +=1

