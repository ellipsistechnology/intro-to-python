#!/user/bin/env python3
import sys
if len(sys.argv)==1:
   print("Hello world!")   #display hello world without typing characters
elif len(sys.argv)==2:
     print("Hello %s!" % (sys.argv[1]))  #display a set of characters input
else:
   i=1
   j=len(sys.argv)
   while i<j:
      print("Hello %s!" % (sys.argv[i]))
      i+=1
