#!/usr/bin/env python3
import sys
if len(sys.argv)==1:      #Determine string length.
   print("Hello world")   #Export hello world.
elif len(sys.argv)==2:    #Determine string length.
     print("Hello %s!"%(sys.argv[1]))  #Displays the input word.
else:
    i=1
    j=len(sys.argv)   #Defines the length of the text.
    while i<j:
        print("Hello %s!"%(sys.argv[i]))    #Loop out consecutive words.
        i+=1
