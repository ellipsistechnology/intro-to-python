#!/usr/bin/env python3
import sys
j=int(sys.argv[1])         #Change a command-line argument to an integer.
m=0
def printThing(x,thing):   #Write a function called printThing(x,thing).
    if m == j-1:
        return "%d %s" %(j-m,sys.argv[2])
    else:
        return "%d %s" %(j-m,sys.argv[2]+"s")  #if the number in front of thing is greater than one,add an "s".
if len(sys.argv) == 1:
    print("Error: Need more arguments.")
elif len(sys.argv) == 2:
    number=int(sys.argv[1])         #define the length of text
    for i in range(1,number+1):
        print(i)                    #display the number from 1 to itself
else:      
    while m < j:
        x = j
        thing = str(sys.argv[2])
        z = printThing(x,thing)
        print("%s" %(z))
        m = m+1
