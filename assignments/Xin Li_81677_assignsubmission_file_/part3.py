#!/usr/bin/env python3
f = open('file.txt','r',True)      #Open the original file
o = open('monkeys.txt','w')        #Open the new file
sub = "monkey";                    #Search all the monkeys
i = f.read().count(sub)            #Put the read and calculated data into the new file
o.write("%d monkeys found." %(i))  #Write down the number i in the new file
f.close()                          #close the old file
o.close()                          #close the new file
