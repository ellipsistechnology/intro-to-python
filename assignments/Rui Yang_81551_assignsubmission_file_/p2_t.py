#!/usr/bin/env python3
import sys

#This program requires the user to input numbers or things to get the certain test based on user's input.

if len(sys.argv) == 1:     #Check if there is no argument inputted by the user.
    print("Error:Need more arguments.")
elif len(sys.argv) == 2:    #Check if there is number inputted by the user.
    count = int(sys.argv[1])
    i = 1
    for i in range(1, count+1):  #The for loop print each number in the list.
        print(i)
elif len(sys.argv) == 3:
    number = int(sys.argv[1])
    i = 1
    for i in range(number, 1, -1):
        print("%d %ss" %(i, sys.argv[2]))
    print("1 %s" %(sys.argv[2]))