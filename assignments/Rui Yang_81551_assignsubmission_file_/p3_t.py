#!/usr/bin/env python3
from collections import Counter
import string

#This program import text from file.txt and remove punctuations by using string function.

text = open('file.txt').read()
for i in text:
    if i in string.punctuation:
        text = text.replace(i," ")
text_no_punctuation = text.split()

#Use the Counter function to count the word frequency in the text.
frequency = Counter(text_no_punctuation)

#Output the results to monkey.txt.
output = open('monkey.txt','w')
output_info = str(frequency['monkey'] + frequency['monkeys']) + " monkeys found."
output.write(output_info)