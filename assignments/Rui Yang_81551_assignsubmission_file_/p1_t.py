#!/usr/bin/env python3
import sys

#This program requires the user to input one or more names to get the certain test based on the number of argument.

if len(sys.argv) == 1:        #Check if there is no argument inputtde by the user.
    print("Hello world!")
elif len(sys.argv) == 2:        #Check if there is 1 name inputted by the user.
    print("Hello %s!" %(sys.argv[1]))
else:
    i = 1
    while i < len(sys.argv):    #The while loop print each name in peparate line.
        print("Hello %s!" %(sys.argv[i]))
        i += 1