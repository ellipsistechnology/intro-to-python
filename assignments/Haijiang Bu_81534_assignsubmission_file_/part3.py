#!/usr/bin/env python3
#Part 3 Tests

import re

txt = open('file.txt',"r").read()
monkey_1 = len(re.findall("Monkey",txt))
monkey_2 = len(re.findall("monkey",txt))
monkey_number = monkey_1 + monkey_2
#Get the number of monkey from a file.txt

file_write = open('monkeys.txt','w',encoding = "utf-8")
#Create a new file names monkeys.txt
file_write.write(str(monkey_number) + ' monkeys found.')
