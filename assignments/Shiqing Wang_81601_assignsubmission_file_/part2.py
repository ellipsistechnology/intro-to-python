#!/usr/bin/env python3
import sys
if len(sys.argv)==1:
	print("Error:Need more arguments.")
elif len(sys.argv)==2:
	count=int(sys.argv[1])
	for i in range(0,count+1):
		print("%d"%i)
else:
	count=int(sys.argv[1])
	for i in range(count,0,-1):
		if i==1:
			print("%d %s"%(i,sys.argv[2]))
		else:
			print("%d %ss"%(i,sys.argv[2]))