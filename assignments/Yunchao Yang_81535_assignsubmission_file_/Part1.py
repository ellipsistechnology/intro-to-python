#!/usr/bin/env python3
import sys
#this program need user to input on or several names 
if len(sys.argv) == 1:#check if there is no argument
    print("Hello world!")
elif len(sys.argv) == 2:#check if there is 1 name inputted
    print("Hello %s!" %(sys.argv[1]))
else:
    i = 1
    while i < len(sys.argv):#use loop to print each name in separate line
        print("Hello %s!" %(sys.argv[i]))
        i += 1