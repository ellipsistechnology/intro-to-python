#!/usr/bin/env python3
#an interesting program for text 1

import sys
i = 1
A = len(sys.argv)
if A == 1:
	print('Hello world!')
#No arguments. Prints "Hello world!"

elif A == 2:
	print('Hello ' + sys.argv[1].title() + '!')
#One argument <name>. Prints "Hello <name>!"

else:
	while i < A:
		print("Hello " + sys.argv[i].title() + "!")
		i += 1
#Multiple arguments; e.g.<name1> <name2> <name3>
#Prints the following lines: "Hello <name1>!" "Hello <name2>!" "Hello <name3>!"
