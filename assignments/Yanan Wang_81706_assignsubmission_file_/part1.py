#!/usr/bin/env python3
# for Part 1 Tests

import sys
i=1

judgement = len (sys.argv)
if judgement == 1:
    print('Hello world!' )
#NO arguments. Prints "Hello world!"
elif judgement == 2:
    print('Hello '+ sys.argv[1]. title() + '!')

#One argument <name>. Prints "Hello <name>!"Eelse:
else:
      while i < judgement:
        print("Hello " + sys.argv[i].title() + "!")
        i +=1

#Multiple arguments; e.g. <name1> <name2> <name 3>
#Prints the following lines: "Hello <namel>!" "Hello <name2>!" "Hello <name3>!"

