#!/usr/bin/env python3
import sys
if len(sys.argv)==1:        # Determine string length
    print("Hello World!")   
elif len(sys.argv)==2:      # Determine string length
     print("Hello %s!" % (sys.argv[1]))   # Displays the input text
else:
    i=1
    j=len(sys.argv)         # Assign text length to 
    while i<j:              
        print("Hello %s!" % (sys.argv[i]))   # Display each text in its corresponding location
        i+=1
