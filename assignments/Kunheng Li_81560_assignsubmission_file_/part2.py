#!/usr/bin/env python3
import sys
j=int(sys.argv[1])
m=0
def printThing(x,thing):       # Define a function called printThing
    if m == j-1:               # Judge the number of texts in j 
        return "%d %s" %(j-m,sys.argv[2])    # 'j-m' is the number, 'sys.argv' is the second text
    else:
        return "%d %s" %(j-m,sys.argv[2]+"s")     # Add 's' to display text greater than 2
if len(sys.argv) == 1:            # This is the case where no text is entered
    print("Error: Need more arguments.")
elif len(sys.argv) == 2:
    number=int(sys.argv[1])      # Create a variable called 'number'
    for i in range(1,number+1):     
        print(i)                 # Print all the numbers from '1' to 'number + 1' 
else:      
    while m < j:
        x = j
        thing = str(sys.argv[2])
        z = printThing(x,thing)     # Substitution function
        print("%s" %(z))
        m = m+1
