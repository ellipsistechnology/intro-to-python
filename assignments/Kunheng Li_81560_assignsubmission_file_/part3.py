#!/usr/bin/env python3
f = open('file.txt','r',True)           # Open the 'file.txt' text
o = open('monkeys.txt','w')             # Open the 'monkeys.txt' text
sub = "monkey";                         # Help to find the number of 'monkey'
i = f.read().count(sub)                 # Use read() and count() to find the number of monkey
o.write("%d monkeys found." %(i))       # Write message in 'monkeys.txt'
f.close()
o.close()
