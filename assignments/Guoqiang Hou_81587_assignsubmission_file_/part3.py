h=open('file.txt','r')
n=open('monkey.txt','w')#open函数的作用是打开一个文件，eg.monkey是文件名，而w是打开文件的模式，w：打开一个文件只用于写入。
s=h.read()#从文件指针所在位置读到文件结尾
sub="monkey"#赋值
i=s.count(sub)#用于统计文件s中monkey出现的次数
m="monkeys found"
n.write("%d %s"%(i,m))#用于向n文件中写入monkey出现的次数和monkey found字符串
h.close()#关闭文件
n.close()

