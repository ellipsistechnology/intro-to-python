#!/usr/bin/env python3
#for Part 2 Tests

import sys
def printThing(x, thing):
	x == 0
	thing == 0
if len(sys.argv)==1:
    print("Error: Need more arguments.")
#No arguments. Prints “Error: Need more arguments.” 
elif len(sys.argv)==2:
	i = 1
	while i <= int(sys.argv[1]):
		print(i)
		i += 1
#One argument<count>. Prints numbers from 1 to count on one line each number. 
elif len(sys.argv)==3:
	i = int(sys.argv[1])
	while i >= 1:
		if i == 1:
			print(str(i) + " " + sys.argv[2])
			i -= 1
		else:
			print(str(i) + " " + sys.argv[2]+"s")
			i -= 1
#Two arguments: <count> <thing>
#Prints “<number> <thing>”if number is singular and “<number> <thing>s” if number is greater than 1,
#with one line for each number and number ranging from count to 1.

