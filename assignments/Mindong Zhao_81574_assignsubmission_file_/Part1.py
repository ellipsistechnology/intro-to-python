#!/usr/bin/env python3
#for part 1 Tests

import sys
i = 1
a = len(sys.argv)
if a == 1:
   print('Hello world!')
#No argument <name>. Prints "Hello <name>!"
elif a == 2:   
   print('Hello ' + sys.argv[1].title() + '!')
else:
     while i < a :
        print("Hello " + sys.argv[i].title() + "!")
        i += 1
#Multiple arguments; e.g.<name1> <name2> <name3>         