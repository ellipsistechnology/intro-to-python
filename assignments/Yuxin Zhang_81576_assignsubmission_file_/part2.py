#!/usr/bin/env python3
#Branch structure
#If there is no input
import sys
if len(sys.argv)==1:
    print("Error:Need more arguments.")                            
elif len(sys.argv)==2:
    i=1
    #Use while loop
    while(i<=int(sys.argv[1])):
        print("%d"%(i))
        i=i+1                                                       
else:
    #Use while loop
    while (int(sys.argv[1])>1):
        print("%d %ss"%(int(sys.argv[1]),sys.argv[2]))
        sys.argv[1]=int(sys.argv[1])-1
    print("%d %s"%(1,sys.argv[2]))                                 