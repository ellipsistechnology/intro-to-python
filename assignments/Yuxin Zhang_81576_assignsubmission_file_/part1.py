#/user/bin/env python3
import sys
if len(sys.argv)==1:
    print("Hello world!")                         #No arguments:Prints"Hello world"
elif len(sys.argv)==2:
    print("Hello <%s>!"%sys.argv[1])              #One argument<name>:Prints"Hello<name>!"  
else :
    for i in range (1,len(sys.argv)):             #deal with Multiple arguments
     print("Hello<%s>!"%(sys.argv[i]))
     i=i+1
             