import sys#调用库文件sys
if len(sys.argv)==1:#本程序段采用了三段条件判断句，分别以if，elif,elif开头，如果三个条件都不满足则执行else后的程序段
    print("Hello world!")
elif len(sys.argv)==2:
    print("Hello %s!"%(sys.argv[1])#若满足sys.avgr[1]字符长度为2，则按字符型变量输出sys.argv[1]。
elif len(sys.argv)==3:#上同。
      i=1
      while i<3:
         names=[sys.argv[i]]
         for x in names:       
             print("Hello %s !"%(x))#在sys.argv[1]和sys.argv[2]中（即sys.argv中提取前两个字符）找x如果条件成立则执行print语句
         i+=1
else:
      i=1
      while i<4:
         names=[sys.argv[i]]#上同，提取字符串前三个，并判断其中x的个数并且输出所有的x组合。
         for x in names:       
             print("Hello %s !"%(x))#循环判断在sys.argv[i]中有没有x，如果在names中有x则执行循环输出，当不满足条件或执行完所有程序时执行计数程序：i+=1，共计循环3次
         i+=1
