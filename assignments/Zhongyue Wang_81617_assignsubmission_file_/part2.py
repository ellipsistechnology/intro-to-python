import sys
if len(sys.argv)==1:
    print("Error:Needmorearguments")#如果if条件成立则执行该语句，并且在本句输出“eorror......"字符串。
elif len(sys.argv)==2:
    count=int(sys.argv[1])#如果elif之后的条件成立则执行以下语句，并且将sys.argv[1]转化成int赋值给count。
    for i in range(1,count,1):#循环输出i值，for循环range后面的参数前两个为左闭右开区间，最后一个参数为迭代的步长。
     print(i)
else:
   count=int(sys.argv[1])#若elif和if条件都不成立则执行else后面的程序，转化成整型变量赋值给count。
   thing=sys.argv[2]
   i=count
   while i>=2:
      print("%d %ss"%(i,thing))#while循环输出结果，其中%d,%ss代表了i，thing的数据类型
      i-=1
   print("%d %s"%(i,thing))#输出最终结果:i,thing

