#!/usr/bin/env python3
import sys
if len(sys.argv) == 1:
    print("Hello world!")
else:
    m = len(sys.argv)
    n = 1
    while m > n:
        print("Hello %s!" %(sys.argv[n]))
        n+=1