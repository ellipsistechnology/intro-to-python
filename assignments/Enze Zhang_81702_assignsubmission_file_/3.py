#!/usr/bin/env python3
f1=open ('file.txt',encoding='utf-8')   # open the file
m=f1.read()                             # file operations
print(m)
n=m.count("monkey")                     # file operations       
print(n)
f2=open(r"monkeys.txt","w")             # open the file
txt="%d monkeys found."%(n)
f2.write(txt)
f1.close()                              # close the file
f2.close()