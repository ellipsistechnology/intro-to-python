#!/usr/bin/env python3 
import sys
if len(sys.argv)==1:
    print("Error:Need more arguments.")
elif len(sys.argv)==2:
    count=int(sys.argv[1])
    for i in range(1,count+1):
      print(i)
else:
    count=int(sys.argv[1])
    for j in range(count,0,-1):
      if j>1:
          print("%d %ss"%(j,sys.argv[2]))
      else:
          print("%d %s"%(j,sys.argv[2]))
