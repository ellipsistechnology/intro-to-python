#!/usr/bin/env python3
def id():
    id="201803201109"        #Student ID
    name="Eric"               #Student name
    cohort="Electrical"      #Must be Electrical or Communications
    return (id,name,cohort)
