#!/usr/bin/env python3
import sys

#This program require user to input number or things, and program will response with different output based on user's input. 

#Define printThing function.
def printThing(x, thing):
    number = int(x)
    i = 1
    for i in range(x, 1, -1):      #Traverse every number in the list in reverse direction and print from large to small number.
        print("%d %ss" %(i, thing))
    print("1 %s" %(thing))         #Print the last result without "s".

if len(sys.argv) == 1:             #Determine if there is no argument given by user.
    print("Error:Need more arguments.")
elif len(sys.argv) == 2:           #Determine if there is number given by user.
    count = int(sys.argv[1])
    i = 1
    for i in range(1,count+1):     #Traverse every number in the list and print these number.
        print(i)
elif len(sys.argv) == 3:           #Determine if there are number and thing given by user.
    printThing(int(sys.argv[1]), sys.argv[2])
    