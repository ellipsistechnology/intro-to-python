#!/usr/bin/env python3

from collections import Counter
import string

#Read the text in file.txt and process the content.
text = open('file.txt').read()      #Import text from file.txt.
for i in text:
    if i in string.punctuation:     #Remove punctuations by using string module.
        text = text.replace(i," ")  #Replace punctuations to space with replace function.
text_no_punctuation=text.split()

#Count the word frequency in the given text with Counter function.
frequency = Counter(text_no_punctuation)

#Export results to monkey.txt.
output = open('monkey.txt', 'w')
output_info = str(frequency['monkey']+frequency['monkeys']) + " monkeys found."
output.write(output_info)