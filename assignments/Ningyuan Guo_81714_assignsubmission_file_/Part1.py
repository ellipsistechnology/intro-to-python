#!/usr/bin/env python3
import sys

#This program need user to input one or several names and print certain text based on the number of arguments.

if len(sys.argv) == 1:  #Check if there is no argument.
    print("Hello world!")
elif len(sys.argv) == 2:    #Check if there is 1 name inputted.
    print("Hello %s!" %(sys.argv[1]))
else:
    i=1
    while i < len(sys.argv):    #Use loop to print each name in separate line.
        print("Hello %s!" %(sys.argv[i]))
        i += 1