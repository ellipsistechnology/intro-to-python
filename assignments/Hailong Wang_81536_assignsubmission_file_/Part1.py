#!/usr/bin/env python3
#part1 test
import sys
j=1
i=len(sys.argv)
if i== 1:
    print("Hello world!") #No arguments. Prints"Helllo world!"
elif i== 2:
    print("Hello %s!" %(sys.argv[1])) #One arguments. Prints"Hello <name>!"
else:
    while j<i:
        print("Hello %s!\n" %(sys.argv[j]))
        j +=1
        #Multiple arguments ;e.g.<name 1><name 2><name 3>. Prints the following lines:
        # "Hello <name 1>!"
        # "Hello <name 2>!"
        # "Hello <name 3>!"