#!usr/bin/env python3
def id():
    id = "201803203131"        # Student ID
    name = "Meng Fangzhou"     # Student name
    cohort = "Communications"  # Must be Electrical or Communications
    return(id, name, cohort)
