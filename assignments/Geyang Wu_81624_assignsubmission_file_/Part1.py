#!/usr/bin/env python3
# the test of Part1
import sys
i=1
if len(sys.argv) == 1:
    print("Hello world!")
#No arguments. Prints "Hello world!"
elif len(sys.argv)==2:
	print("Hello %s!"%(sys.argv[i]))
#One argument<name>; Prints "Hello <name>!"
else:
    while i<len(sys.argv):
       print("Hello %s!"%(sys.argv[i]))
       i+=1
 #multiple arguments;a few <name>.
 # Prints "Hello <name1>!""Hello <name2>!"in each lines

