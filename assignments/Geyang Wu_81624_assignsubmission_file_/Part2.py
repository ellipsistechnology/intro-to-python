#!/usr/bin/env python3
# the test of Part2
import sys
def func1():
	print ("Error: Need more arguments.")
def func2(count):
	count = int(sys.argv[1])
	for i in range (1,count+1):
		print (i)
def printThing(x, thing):
	x= int(sys.argv[1])
	for i in range (2,x+1):
		print (str(x),thing+"s")
		x=x-1
	print (str(x),thing)
if len(sys.argv) == 1:
	func1()
# No argumets, prints "Error: Need more arguments."
elif len(sys.argv)==2:
	func2(sys.argv[1])
# One argument, prints numbers from 1 to count on one line for each
elif len(sys.argv)==3:
	printThing (sys.argv[1],sys.argv[2])
#Two arguments:<count>and<thing>
#Prints <number><things> in each line in case of the <number> being greater than 1
#For 1 line each to range the number from <number> to one
