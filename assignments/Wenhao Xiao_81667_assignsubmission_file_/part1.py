#!/usr/bin/env python3
import sys
if len(sys.argv) ==1:
 print("Hello World!") # Output "Hello world!"
elif len(sys.argv) ==2:
 print("Hello %s!" %(sys.argv[1])) # Output "Hello someone name!"
elif len(sys.argv) >2:
 i=1 # Assign a value to i
 while i<len(sys.argv): # Loop when less than the set parameter
    print("Hello %s!"%(sys.argv[i])) # Output "Hello someone name!"line by line
    i+=1 # Add 1 to each cycle