#!/usr/bin/env python3
f=open (r"file.txt") # Open file and assign value to f
x=f.read() # Read the file and assign it to x
print(x) # Output x
n=x.count("monkey") # Retrieves the number of occurrences of "monkey" in x and assigns it to n
f1=open(r"monkeys.txt","w") # Open or creat a file called "monkeys.txt" Make it modifiable and assign it to f1
txt="%d monkeys found."%(n) # Assign a value to txt
f1.write(txt) # Write the value attached to txt in f1
f.close() # Close file
f1.close() # Close file