#!/usr/bin/env python3
import sys
if len(sys.argv) ==1:
    print("Error:Need more arguments.") # Output "Error:Need more arguments."
elif len(sys.argv) ==2:
    c=int(sys.argv[1]) # Assign a value to c
    i=1 # Assign a value to i
    for i in range(1,c+1): # Bring i into the array to loop
     print(i) # Output i
if len(sys.argv) ==3:
    x=1 # Assign a value to x
    for x in range(int(sys.argv[1]),0,-1): # Bring x into the array to loop
     if x>1:
        print("%d %ss"%(x,sys.argv[2])) # Ouput when x is greater than 1
     elif x==1:
        print("%d %s"%(x,sys.argv[2])) # Output when x equals 1
